package com.ggardet.todo.resources.blogpost.controller;

import com.ggardet.todo.resources.blogpost.service.BlogPostService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@EnableWebMvc
@AutoConfigureMockMvc
@WebAppConfiguration
@SpringBootTest
class BlogPostControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private BlogPostService blogPostService;

    @WithMockUser
    @Test
    void getBlogPostsIds() throws Exception {
        final var ids = List.of(9);
//        Mockito.when(blogPostService.fetchPostsIds()).thenReturn(ids);
        this.mockMvc.perform(
                        get("/posts")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", isA(ArrayList.class)))
                .andExpect(jsonPath("$[0]", equalTo(ids.get(0))));
    }
}
