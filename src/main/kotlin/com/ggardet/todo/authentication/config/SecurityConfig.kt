package com.ggardet.todo.authentication.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.User
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.security.web.SecurityFilterChain

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
class SecurityConfig {
    private val userDetails = User
        .withUsername("user")
        .password("{noop}user")
        .roles("USER")
        .build()

    private val adminDetails = User
        .withUsername("admin")
        .password("{noop}admin")
        .roles("ADMIN", "USER")
        .build()

    @Bean
    fun apiSecurityFilterChain(httpSecurity: HttpSecurity): SecurityFilterChain {
        val userDetailsService = InMemoryUserDetailsManager(userDetails, adminDetails)
        httpSecurity
            .authorizeRequests()
            .anyRequest()
            .authenticated()
        httpSecurity.httpBasic(Customizer.withDefaults())
        httpSecurity.userDetailsService(userDetailsService)
        httpSecurity.csrf().disable()
        httpSecurity.cors().disable()
        return httpSecurity.build()
    }
}
