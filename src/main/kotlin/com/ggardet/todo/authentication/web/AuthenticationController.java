package com.ggardet.todo.authentication.web;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

import static org.springframework.boot.context.properties.bind.Bindable.mapOf;

@RestController
public class AuthenticationController {

    @GetMapping("/me")
    public ResponseEntity<Map<String, Object>> getAuthenticatedUser(
            @AuthenticationPrincipal final UserDetails userDetails
    ) {
        final var response = Map.of(
                "name", userDetails.getUsername(),
                "authorities", userDetails.getAuthorities()
        );
        return ResponseEntity.ok(response);
    }
}
