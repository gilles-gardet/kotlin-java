package com.ggardet.todo.resources.todo.service

import com.ggardet.todo.resources.project.repository.ProjectRepository
import com.ggardet.todo.resources.todo.domain.dto.TodoDto
import com.ggardet.todo.resources.todo.domain.entity.Todo
import com.ggardet.todo.resources.todo.mapper.TodoMapper
import com.ggardet.todo.resources.todo.repository.TodoRepository
import com.ggardet.todo.search.service.SearchService
import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*
import javax.persistence.EntityNotFoundException

@Service
class TodoService(
    val todoMapper: TodoMapper,
    val todoRepository: TodoRepository,
    val projectRepository: ProjectRepository,
    val searchService: SearchService<Todo>
) {
    companion object {
        private val log = LoggerFactory.getLogger(this::class.java)
    }

    fun persistTodo(todoDto: TodoDto): String {
        log.info("Persisting todo into database")
        val todo = todoMapper.toEntity(todoDto)
        todo.project?.id?.let { setParentProject(it, todo) }
        return todoRepository.save(todo).uuid.toString()
    }

    private fun setParentProject(id: Long, todo: Todo) {
        log.info("Attaching parent project $id to the todo")
        val project = projectRepository.findByIdOrNull(id)
        todo.project = project
    }

    fun fetchTodo(todoIdentifier: UUID): TodoDto {
        log.info("Fetching todo $todoIdentifier from database")
        val todo = todoRepository.findByIdOrNull(todoIdentifier)
        return todo.let {
            todoMapper.toDto(it ?: throw EntityNotFoundException("Todo not found for identifier $todoIdentifier"))
        }
    }

    fun fetchTodos(): List<TodoDto> {
        log.info("Fetching todos from database")
        val todos = todoRepository.findAll()
        return todoMapper.toDtos(todos)
    }

    fun searchTodo(text: String): List<TodoDto> {
        log.info("Searching todo from database")
        val todos = searchService.searchMultipleMatchesBy(Todo::class.java, text, "title")
        return todoMapper.toDtos(todos)
    }

    fun modifyTodo(todoIdentifier: UUID, todoDto: TodoDto) {
        log.info("Modifying todo $todoIdentifier in database")
        val todo = todoRepository.findByIdOrNull(todoIdentifier)
        val response = todo.takeIf { todo != null }
            ?: throw EntityNotFoundException("Todo not found for identifier $todoIdentifier")
        todoMapper.toEntity(response, todoDto)
        todoRepository.save(response)
    }

    fun removeTodo(todoIdentifier: UUID) {
        log.info("Removing todo $todoIdentifier from database")
        todoRepository.deleteById(todoIdentifier)
    }
}
