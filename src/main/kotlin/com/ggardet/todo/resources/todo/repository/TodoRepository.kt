package com.ggardet.todo.resources.todo.repository

import com.ggardet.todo.resources.todo.domain.entity.Todo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface TodoRepository: JpaRepository<Todo, UUID>
