package com.ggardet.todo.resources.todo.domain.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL
import com.ggardet.todo.resources.project.domain.dto.ProjectDto
import java.util.UUID
import javax.validation.constraints.NotBlank

data class TodoDto(
    var identifier: UUID?,

    @field:NotBlank var name: String,

    var completed: Boolean = false,

    @field:JsonInclude(NON_NULL) var project: ProjectDto?
)
