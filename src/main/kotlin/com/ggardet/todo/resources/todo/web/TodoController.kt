package com.ggardet.todo.resources.todo.web

import com.ggardet.todo.resources.todo.domain.dto.TodoDto
import com.ggardet.todo.resources.todo.service.TodoService
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.util.*
import javax.validation.Valid

@RequestMapping("/todos")
@RestController
class TodoController(val todoService: TodoService) {
    @PostMapping
    fun createTodo(@Valid @RequestBody todoDto: TodoDto): ResponseEntity<Unit> {
        val uri = todoService.persistTodo(todoDto).let {
            ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{uuid}")
                .build(it)
        }
        return ResponseEntity.created(uri).build()
    }

    @GetMapping("/{todoIdentifier}")
    fun readTodo(@PathVariable todoIdentifier: UUID) = todoService.fetchTodo(todoIdentifier)

    @GetMapping
    fun readTodos() = todoService.fetchTodos()

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/search")
    fun searchTodo(@RequestParam text: String) = todoService.searchTodo(text)

    @PutMapping("/{todoIdentifier}")
    fun updateTodo(@PathVariable todoIdentifier: UUID, @Valid @RequestBody todoDto: TodoDto): ResponseEntity<Unit> {
        todoService.modifyTodo(todoIdentifier, todoDto)
        return ResponseEntity.noContent().build()
    }

    @DeleteMapping("/{todoIdentifier}")
    fun deleteTodo(@PathVariable todoIdentifier: UUID): ResponseEntity<Unit> {
        todoService.removeTodo(todoIdentifier)
        return ResponseEntity.noContent().build()
    }
}
