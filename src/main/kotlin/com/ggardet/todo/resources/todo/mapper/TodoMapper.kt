package com.ggardet.todo.resources.todo.mapper

import com.ggardet.todo.resources.project.mapper.ProjectMapper
import com.ggardet.todo.resources.todo.domain.dto.TodoDto
import com.ggardet.todo.resources.todo.domain.entity.Todo
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.MappingTarget
import org.mapstruct.NullValuePropertyMappingStrategy

@Mapper(uses = [ProjectMapper::class])
interface TodoMapper {
    @Mapping(target = "name", source = "title")
    @Mapping(target = "identifier", source = "uuid")
    fun toDto(todo: Todo): TodoDto

    fun toDtos(todos: List<Todo>): List<TodoDto>

    @Mapping(target = "title", source = "name")
    @Mapping(
        target = "uuid",
        source = "identifier",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
    )
    fun toEntity(todoDto: TodoDto): Todo

    @Mapping(target = "title", source = "name")
    @Mapping(
        target = "uuid",
        source = "identifier",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
    )
    fun toEntity(@MappingTarget todo: Todo, todoDto: TodoDto)
}
