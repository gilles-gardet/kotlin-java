package com.ggardet.todo.resources.blogpost.domain;

import lombok.Data;

@Data
public class BlogPost {
    private int userId;
    private String title;
    private String body;
    private int id;
}
