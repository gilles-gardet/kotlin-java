package com.ggardet.todo.resources.blogpost.service

import com.ggardet.todo.resources.blogpost.domain.BlogPost
import lombok.extern.slf4j.Slf4j
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient

@Slf4j
@Service
class BlogPostService(val webClient: WebClient) {
    fun createPost(blogPost: BlogPost) =
        webClient.post()
            .body(BodyInserters.fromValue(blogPost))
            .retrieve()
            .toEntity(BlogPost::class.java)
            .block() ?: throw Exception("Unable to create post")

    fun fetchPost(postId: Int): ResponseEntity<BlogPost> =
        webClient.get()
            .uri("/{postId}", postId)
            .retrieve()
            .toEntity(BlogPost::class.java)
            .block() ?: throw Exception("No post found for id $postId")

    fun fetchPostsIds(): List<Int> =
        webClient.get()
            .retrieve()
            .toEntityList(BlogPost::class.java)
            .mapNotNull { it.body?.map { obj: BlogPost -> obj.id } }
            .block() ?: throw Exception("No posts found")

    fun updatePost(postId: Int, blogPost: BlogPost): ResponseEntity<BlogPost> {
        val placeholderBlogPost = BlogPost()
            .apply { id = postId; title = blogPost.title }
        return webClient.put()
            .uri("/{postId}", postId)
            .body(BodyInserters.fromValue(placeholderBlogPost))
            .retrieve()
            .toEntity(BlogPost::class.java)
            .block() ?: throw Exception("No post found for id $postId")
    }

    fun deletePost(postId: Int) =
        webClient.delete()
            .uri("/{postId}", postId)
            .retrieve()
            .toEntity(BlogPost::class.java)
            .block() ?: throw Exception("No post found for id $postId")
}
