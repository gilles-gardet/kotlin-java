package com.ggardet.todo.resources.blogpost.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import io.netty.handler.logging.LogLevel
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.client.reactive.ClientHttpConnector
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.http.codec.ClientCodecConfigurer
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import reactor.netty.transport.logging.AdvancedByteBufFormat


@Configuration
class BlogPostWebClientConfig {
    companion object {
        private const val JSON_PLACEHOLDER_API = "https://jsonplaceholder.typicode.com/posts"
    }

    @Bean
    fun exchangeStrategies(): ExchangeStrategies {
        val objectMapper = ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return ExchangeStrategies
            .builder()
            .codecs { configurer: ClientCodecConfigurer ->
                configurer.defaultCodecs().jackson2JsonDecoder(Jackson2JsonDecoder(objectMapper))
            }
            .build()
    }

    fun clientHttpConnector(): ClientHttpConnector {
        val httpClient: HttpClient = HttpClient.create().wiretap(
            this.javaClass.canonicalName, LogLevel.DEBUG,
            AdvancedByteBufFormat.TEXTUAL
        )
        return ReactorClientHttpConnector(httpClient)
    }

    @Bean
    fun webClient(strategies: ExchangeStrategies): WebClient =
        WebClient.builder()
            .exchangeStrategies(strategies)
            .clientConnector(clientHttpConnector())
            .baseUrl(JSON_PLACEHOLDER_API)
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build()
}
