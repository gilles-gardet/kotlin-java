package com.ggardet.todo.resources.blogpost.controller;

import com.ggardet.todo.resources.blogpost.domain.BlogPost;
import com.ggardet.todo.resources.blogpost.service.BlogPostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/posts")
public class BlogPostController {
    private final BlogPostService blogPostService;

    @PostMapping
    public ResponseEntity<BlogPost> createPost(@RequestBody final BlogPost blogPost) {
        log.info("Create a new post {}", blogPost.toString());
        return blogPostService.createPost(blogPost);
    }

    @GetMapping(value = "/{blogPostId}")
    public ResponseEntity<BlogPost> readPost(@PathVariable final int blogPostId) {
        log.info("Fetch the details for post {}", blogPostId);
        return blogPostService.fetchPost(blogPostId);
    }

    @GetMapping
    public List<Integer> readPosts() {
        log.info("Fetch the posts");
        return blogPostService.fetchPostsIds();
    }

    @PutMapping(value = "/{blogPostId}")
    public ResponseEntity<BlogPost> updatePost(
            @RequestBody final BlogPost blogPost,
            @PathVariable final int blogPostId
    ) {
        log.info("Update post {}", blogPostId);
        return blogPostService.updatePost(blogPostId, blogPost);
    }

    @DeleteMapping(value = "/{blogPostId}")
    public ResponseEntity<Void> deletePost(@PathVariable final int blogPostId) {
        log.info("Delete post {}", blogPostId);
        blogPostService.deletePost(blogPostId);
        return ResponseEntity.noContent().build();
    }
}
