package com.ggardet.todo.resources.project.mapper;

import com.ggardet.todo.resources.project.domain.dto.ProjectDto;
import com.ggardet.todo.resources.project.domain.entity.Project;
import org.mapstruct.Mapper;

@Mapper
public interface ProjectMapper {
    Project toEntity(final ProjectDto projectDto);

    ProjectDto toDto(final Project project);
}
