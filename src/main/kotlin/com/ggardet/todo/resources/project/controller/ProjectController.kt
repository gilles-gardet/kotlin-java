package com.ggardet.todo.resources.project.controller

import com.ggardet.todo.resources.project.domain.dto.ProjectDto
import com.ggardet.todo.resources.project.service.ProjectService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import javax.validation.Valid

@RequestMapping("/projects")
@RestController
class ProjectController(val projectService: ProjectService) {
    @PostMapping
    fun createProject(@Valid @RequestBody projectDto: ProjectDto): ResponseEntity<Unit> {
        val uri = projectService.persistProject(projectDto).let {
            ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{uuid}")
                .build(it)
        }
        return ResponseEntity.created(uri).build()
    }

    @GetMapping("/{projectIdentifier}")
    fun readProject(@PathVariable projectIdentifier: Long): ProjectDto = projectService.fetchProject(projectIdentifier)

    @GetMapping
    fun readProjects(): MutableCollection<Long> = projectService.fetchProjects()

    @PutMapping("/{projectIdentifier}")
    fun updateProject(
        @PathVariable projectIdentifier: Long,
        @Valid @RequestBody projectDto: ProjectDto
    ): ResponseEntity<Unit> {
        projectService.modifyProject(projectIdentifier, projectDto)
        return ResponseEntity.noContent().build()
    }

    @DeleteMapping("/{projectIdentifier}")
    fun deleteProject(@PathVariable projectIdentifier: Long): ResponseEntity<Unit> {
        projectService.removeProject(projectIdentifier)
        return ResponseEntity.noContent().build()
    }
}
