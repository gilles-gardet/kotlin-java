package com.ggardet.todo.resources.project.service;

import com.ggardet.todo.resources.project.domain.dto.ProjectDto;
import com.ggardet.todo.resources.project.domain.entity.Project;
import com.ggardet.todo.resources.project.mapper.ProjectMapper;
import com.ggardet.todo.resources.project.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    public long persistProject(final ProjectDto projectDto) {
        final var project = projectMapper.toEntity(projectDto);
        final var createdProject = projectRepository.save(project);
        return createdProject.getId();
    }

    public ProjectDto fetchProject(final long projectId) {
        final var project = projectRepository.findById(projectId);
        if (project.isEmpty()) {
            throw new EntityNotFoundException("Project not found");
        }
        return projectMapper.toDto(project.get());
    }

    public Collection<Long> fetchProjects() {
        final var projects = projectRepository.findAll();
        if (projects.isEmpty()) {
            throw new EntityNotFoundException("No projects found");
        }
        return Stream.of(projects)
                .flatMap(Collection::stream)
                .map(Project::getId)
                .collect(Collectors.toList());
    }

    public void modifyProject(final long projectId, final ProjectDto projectDto) {
        final var project = projectRepository.findById(projectId);
        if (project.isEmpty()) {
            throw new EntityNotFoundException("Project not found");
        }
        final var projectToUpdate = project.get();
        projectToUpdate.setName(projectDto.getName());
        projectRepository.save(projectToUpdate);
    }

    public void removeProject(final long projectId) {
        projectRepository.deleteById(projectId);
    }
}
