package com.ggardet.todo.resources.project.domain.dto;

import lombok.Data;

@Data
public class ProjectDto {
    private String name;
}
