package com.ggardet.todo.core.error

import com.ggardet.todo.search.domain.SearchResultException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.persistence.EntityNotFoundException

@ControllerAdvice
class RestErrorHandler : ResponseEntityExceptionHandler() {
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    override fun handleMethodArgumentNotValid(
        methodArgumentNotValidException: MethodArgumentNotValidException,
        httpHeaders: HttpHeaders,
        httpStatus: HttpStatus,
        webRequest: WebRequest
    ) = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(mapOf("message" to "Invalid request") as Any)

    @ExceptionHandler(org.springframework.security.access.AccessDeniedException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun handleAccessDeniedException(
        accessDeniedException: org.springframework.security.access.AccessDeniedException
    ) = ResponseEntity.status(HttpStatus.FORBIDDEN).body(mapOf("message" to "Access refused"))

    @ExceptionHandler(EntityNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleEntityNotFoundException(entityNotFoundException: EntityNotFoundException) =
        ResponseEntity.status(HttpStatus.NOT_FOUND).body(
            mapOf("message" to "The requested resource was not found")
        )

    @ExceptionHandler(SearchResultException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleEntityNotFoundException(searchResultException: SearchResultException) =
        ResponseEntity.status(HttpStatus.NOT_FOUND).body(mapOf("message" to searchResultException.message))

    @ExceptionHandler(ClassCastException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleClassCastException(classCastException: ClassCastException) =
        ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
            mapOf("message" to "Error while processing the request")
        )

    @ExceptionHandler(Exception::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleExceptionException(exception: Exception) = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
        mapOf("message" to "Unknown error")
    )
}
