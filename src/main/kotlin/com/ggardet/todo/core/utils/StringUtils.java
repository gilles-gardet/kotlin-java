package com.ggardet.todo.core.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class StringUtils {

    public static String objectToString(@NonNull Object object) {
        if (object instanceof String) {
            return (String) object;
        }
        return object.toString();
    }
}
