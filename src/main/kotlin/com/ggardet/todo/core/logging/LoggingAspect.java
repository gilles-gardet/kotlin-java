package com.ggardet.todo.core.logging;

import com.ggardet.todo.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Aspect
@Component
public class LoggingAspect {
    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void controllerTypePcd() {
        // pointcut used to matches the Spring's Rest Controllers
    }

    @Pointcut("within(@org.springframework.stereotype.Service *)")
    public void serviceTypePcd() {
        // pointcut used to matches the Spring's Services
    }

    @Pointcut("within(@org.springframework.stereotype.Repository *)")
    public void repositoryTypePcd() {
        // pointcut used to matches the Spring's Repositories
    }

    @Pointcut("controllerTypePcd() || serviceTypePcd() || repositoryTypePcd()")
    public void springBeanPcd() {
        // pointcut used to matches the Spring's Services, Controllers and Repositories
    }

    @Pointcut("springBeanPcd() || within(com.ggardet.todo.resources..*)")
    public void resourcesLayerPcd() {
        // pointcut used to matches the Spring's Services, Controllers and Repositories
    }

    @Before("resourcesLayerPcd() && target(bean)")
    public void logBefore(JoinPoint joinPoint, Object bean) {
        final var pairNames = getMethodAndClassNames(joinPoint);
        final var arguments = mapArgsToList(joinPoint.getArgs());
        if (!log.isTraceEnabled()) {
            return;
        }
        if (arguments.isEmpty()) {
            log.trace("Invoking {}#{}(..)", pairNames.getFirst(), pairNames.getSecond());
        } else {
            final var methodArguments = String.join(", ", arguments);
            log.trace("Invoking {}#{}(..) : {}", pairNames.getFirst(), pairNames.getSecond(), methodArguments);
        }
    }

    @AfterReturning(pointcut = "resourcesLayerPcd()", returning = "object")
    public void logAfterReturning(JoinPoint joinPoint, Object object) {
        final var pairNames = getMethodAndClassNames(joinPoint);
        final var result = Optional.ofNullable(object);
        if (!log.isTraceEnabled()) {
            return;
        }
        result.ifPresentOrElse(
                value -> {
                    var returnValue = StringUtils.objectToString(value);
                    log.trace("Returning {}#{}(..) : {}", pairNames.getFirst(), pairNames.getSecond(), returnValue);
                },
                () -> log.trace("Returning {}#{}(..) : null", pairNames.getFirst(), pairNames.getSecond())
        );
    }

    @Around("resourcesLayerPcd()")
    public Object logMethodExecutionTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        if (!log.isInfoEnabled()) {
            return proceedingJoinPoint.proceed();
        }
        final var stopWatch = new StopWatch();
        stopWatch.start();
        final var proceed = proceedingJoinPoint.proceed();
        stopWatch.stop();
        final var pairNames = getMethodAndClassNames(proceedingJoinPoint);
        log.info("Execution time of {}#{}(..) - {}ms",
                pairNames.getFirst(),
                pairNames.getSecond(),
                stopWatch.getTotalTimeMillis());
        return proceed;
    }

    private static List<String> mapArgsToList(Object[] objects) {
        return Arrays.stream(objects)
                .filter(Objects::nonNull)
                .map(StringUtils::objectToString)
                .collect(Collectors.toList());
    }

    private static Pair<String, String> getMethodAndClassNames(JoinPoint joinPoint) {
        final var methodSignature = (MethodSignature) joinPoint.getSignature();
        final var className = methodSignature.getDeclaringTypeName();
        final var methodName = methodSignature.getName();
        return Pair.of(className, methodName);
    }
}
