package com.ggardet.todo.core.persistence

import org.springframework.data.annotation.AccessType
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import java.time.Instant
import javax.persistence.Embeddable

@AccessType(AccessType.Type.FIELD)
@Embeddable
data class AuditMetadata(
    @CreatedBy
    var creator: String?,

    @LastModifiedBy
    var updater: String?,

    @CreatedDate
    var creationDate: Instant?,

    @LastModifiedDate
    var modificationDate: Instant?
)
