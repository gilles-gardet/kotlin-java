package com.ggardet.todo.search.service;

import com.ggardet.todo.search.domain.SearchResultException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.mapper.orm.Search;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Service
public class SearchService<T> {
    private final EntityManager entityManager;
    private static final int HIT_LIMIT = 10;

    public List<T> searchMultipleMatchesBy(
            final Class<T> searchableClass,
            final String text,
            final String... fields
    ) {
        final var searchSession = Search.session(entityManager);
        final var result = searchSession
                .search(searchableClass)
                .where(predicate -> predicate.match().fields(fields).matching(text).fuzzy(2))
                .fetchHits(HIT_LIMIT);
        if (Objects.isNull(result) || result.isEmpty()) {
            final var errorMessage = String.format("No values were found for term %s", text);
            throw new SearchResultException(errorMessage);
        }
        return result;
    }
}
