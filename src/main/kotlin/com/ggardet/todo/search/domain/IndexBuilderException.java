package com.ggardet.todo.search.domain;

public class IndexBuilderException extends RuntimeException {
    public IndexBuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
