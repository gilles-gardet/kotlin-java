package com.ggardet.todo.search.domain;

public class SearchResultException extends RuntimeException {
    public SearchResultException(String message) {
        super(message);
    }
}
