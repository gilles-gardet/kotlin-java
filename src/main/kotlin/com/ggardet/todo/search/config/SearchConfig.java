package com.ggardet.todo.search.config;

import com.ggardet.todo.search.domain.IndexBuilderException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.mapper.orm.Search;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class  SearchConfig {
    private final EntityManager entityManager;

    @EventListener(ApplicationStartedEvent.class)
    @Transactional
    public void onApplicationStart() {
        log.info("Mass indexing start");
        final var searchSession = Search.session(entityManager);
        searchSession.schemaManager().dropAndCreate();
        try {
            searchSession.massIndexer().startAndWait();
        } catch (final InterruptedException interruptedException) {
            Thread.currentThread().interrupt();
            throw new IndexBuilderException("Mass indexing exception", interruptedException.getCause());
        }
    }
}
