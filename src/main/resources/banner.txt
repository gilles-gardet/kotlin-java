${AnsiColor.MAGENTA}  _  __     _   _ _        ${AnsiColor.YELLOW}        ${AnsiColor.RED}     _
${AnsiColor.MAGENTA} | |/ /___ | |_| (_)_ __   ${AnsiColor.YELLOW}   _    ${AnsiColor.RED}    | | __ ___   ____ _
${AnsiColor.MAGENTA} | ' // _ \| __| | | '_ \  ${AnsiColor.YELLOW} _| |_  ${AnsiColor.RED} _  | |/ _` \ \ / / _` |
${AnsiColor.MAGENTA} | . \ (_) | |_| | | | | | ${AnsiColor.YELLOW}|_   _| ${AnsiColor.RED}| |_| | (_| |\ V / (_| |
${AnsiColor.MAGENTA} |_|\_\___/ \__|_|_|_| |_| ${AnsiColor.YELLOW}  |_|   ${AnsiColor.RED} \___/ \__,_| \_/ \__,_|

${AnsiColor.YELLOW}SpringBoot version: ${spring-boot.formatted-version}
${AnsiColor.DEFAULT}
