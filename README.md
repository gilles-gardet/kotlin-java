#  [Kotlin 🍻 Java](https://presentation.gilles-gardet.com)

Simple project used to test interoperability between **Kotlin** & **Java** code within a Spring project.

Both languages should be able to be called from **one side or another** (meaning Java can import and use Kotlin code and the other way around).  

This project also check if we still can easily use **annotation processor** libraries (such as lombok and mapstruct) which generate Java code at compilation.

## Compatibility check for framework & libraries
- [x] Spring Security
- [x] Spring Data
- [x] Spring WebClient
- [x] Spring AOP
- [x] SpringBoot Actuator
- [x] JUnit
- [x] Hibernate Search (lucene)
- [x] Mapstruct
- [x] Lombok
- [x] Logbook

All libraries are used in both Kotlin and Java code to check interoperability. 

> Even if some libraries are not used in both languages, they are still imported to check if they can be used in both languages.

## Build

To build the project for the first time, use the following command:  
```bash
mvn install -DskipTests
```

In case you need to compile again the project after some changes:
```bash
mvn clean compile
```

## Run

To run the project, use the following command:  
```bash
mvn spring-boot:run
```
For simplicity, the project uses an in-memory H2 database.

A [Postman collection](./data/kotlin+java.postman_collection.json) is available in the `data` folder.

## Test

To run the tests:  
```bash
mvn test
```
